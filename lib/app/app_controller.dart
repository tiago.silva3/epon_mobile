import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';

part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  Color corIntelbras = Color.fromRGBO(12, 122, 47, 1);

  SystemUiOverlayStyle colorAppBar = SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  );

  ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    splashColor: Colors.white,
    primaryColor: Colors.green,
    primarySwatch: Colors.green,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Color.fromRGBO(12, 122, 47, 1),
    ),
    appBarTheme: AppBarTheme(
      elevation: 1,
      color: Color.fromRGBO(253, 253, 253, 1),
      brightness: Brightness.light,
    ),
    cardTheme: CardTheme(
      elevation: 1.5,
    ),
  );

  ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    splashColor: Colors.black,
    primaryColorDark: Colors.green,
    primarySwatch: Colors.green,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Color.fromRGBO(12, 122, 47, 1),
    ),
    appBarTheme: AppBarTheme(
      elevation: 1,
      color: Color.fromRGBO(43, 43, 43, 1),
      brightness: Brightness.dark,
    ),
    cardTheme: CardTheme(
      elevation: 1,
    ),
  );

  @observable
  ThemeMode themeMode = ThemeMode.system;

  @observable
  String valorSystem = "system";

  @observable
  String valorLight = "";

  @observable
  String valorDark = "";

  @action
  void trocarTema(String tema) {
    switch (tema) {
      case "light":
        themeMode = ThemeMode.light;
        valorSystem = "";
        valorLight = "light";
        valorDark = "";
        break;
      case "dark":
        themeMode = ThemeMode.dark;
        valorSystem = "";
        valorLight = "";
        valorDark = "dark";
        break;
      default:
        themeMode = ThemeMode.system;
        valorSystem = "system";
        valorLight = "";
        valorDark = "";
        break;
    }
  }
}
