import 'package:epon_flutter/app/main_module/componentes/popup_user/popup_user.dart';
import 'package:flutter/material.dart';

class NomeIntelbas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text("Intelbras",
            style: TextStyle(color: Color.fromRGBO(12, 122, 47, 1))),
        PopUpUser()
      ],
    );
  }
}
