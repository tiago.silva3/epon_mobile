import 'package:flutter/material.dart';

class CampoPesquisa extends StatefulWidget {
  CampoPesquisa({Key key, this.larguraTela}) : super(key: key);

  @override
  _CampoPesquisaState createState() => _CampoPesquisaState();
  final double larguraTela;
}

class _CampoPesquisaState extends State<CampoPesquisa> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextField(
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Pesquisar",
            focusColor: Colors.transparent),
      ),
    );
  }
}
