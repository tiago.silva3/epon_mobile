import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'popup_user_controller.dart';

class PopUpUser extends StatelessWidget {
  final controller = Modular.get<PopUpUserController>();

  @override
  Widget build(BuildContext context) {
    final tamanhoTela = MediaQuery.of(context).size.width;

    return Container(
      child: CircleAvatar(
        backgroundColor: Color.fromRGBO(128, 128, 128, 0.2),
        child: IconButton(
            highlightColor: Colors.transparent,
            icon: Icon(Icons.person_outline),
            color: Colors.grey,
            tooltip: "Configurações do usuário",
            onPressed: () {
              controller.showModalUser(context, tamanhoTela);
            }),
      ),
    );
  }
}
