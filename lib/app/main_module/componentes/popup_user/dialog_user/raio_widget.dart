import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../app_controller.dart';

class RadiosWidget extends StatelessWidget {
  RadiosWidget({Key key, this.tamanhoTela}) : super(key: key);

  final double tamanhoTela;
  final controller = Modular.get<AppController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: tamanhoTela * 0.03, right: tamanhoTela * 0.03),
          child: Row(
            children: <Widget>[
              Expanded(
                child: ListTile(
                  title: Text("Tema"),
                  leading: Icon(Icons.brush),
                ),
              ),
            ],
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Divider(indent: tamanhoTela * 0.17, color: Colors.grey),
            RadioListTile(
                title: Text("System"),
                value: "system",
                groupValue: controller.valorSystem,
                onChanged: controller.trocarTema,
                activeColor: controller.corIntelbras),
            RadioListTile(
                title: Text("Light"),
                value: "light",
                groupValue: controller.valorLight,
                onChanged: controller.trocarTema,
                activeColor: controller.corIntelbras),
            RadioListTile(
                title: Text("Dark"),
                value: "dark",
                groupValue: controller.valorDark,
                onChanged: controller.trocarTema,
                activeColor: controller.corIntelbras),
          ],
        ),
      ],
    );
  }
}
