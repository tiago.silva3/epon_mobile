import 'package:epon_flutter/app/main_module/componentes/popup_user/dialog_user/raio_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../app_controller.dart';
import 'line_user.dart';

class DialogUser extends StatefulWidget {
  DialogUser({Key key, this.tamanhoTela}) : super(key: key);

  final double tamanhoTela;
  @override
  _DialogUserState createState() => _DialogUserState();
}

class _DialogUserState extends State<DialogUser> {
  final controller = Modular.get<AppController>();
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
            top: widget.tamanhoTela * 0.015,
            left: widget.tamanhoTela * 0.03,
            right: widget.tamanhoTela * 0.03),
        child: LineUser(),
      ),
      Divider(color: Colors.grey),
      RadiosWidget(tamanhoTela: widget.tamanhoTela),
    ]);
  }
}
