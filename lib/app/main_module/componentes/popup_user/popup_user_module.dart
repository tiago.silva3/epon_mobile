import 'package:flutter_modular/flutter_modular.dart';

import 'popup_user_controller.dart';
import 'popup_user.dart';

class PopUpUserModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => PopUpUserController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => PopUpUser()),
      ];

  static Inject get to => Inject<PopUpUser>.of();
}
