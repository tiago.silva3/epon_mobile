import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'dialog_user/dialog_user.dart';

part "popup_user_controller.g.dart";

class PopUpUserController = _PopUpUserControllerBase with _$PopUpUserController;

abstract class _PopUpUserControllerBase with Store {
  void showModalUser(BuildContext context, double tamanhoTela) {
    showModalBottomSheet(
        context: context,
        builder: (_) => DialogUser(tamanhoTela: tamanhoTela),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(tamanhoTela * 0.025),
                topRight: Radius.circular(tamanhoTela * 0.025))));
  }
}
