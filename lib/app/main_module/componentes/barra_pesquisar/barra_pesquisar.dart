import 'package:epon_flutter/app/main_module/componentes/compo_pesquisa/compo_pesquisa.dart';
import 'package:epon_flutter/app/main_module/componentes/popup_user/popup_user.dart';
import 'package:flutter/material.dart';

class BarraPesquisar extends StatefulWidget {
  @override
  _BarraPesquisarState createState() => _BarraPesquisarState();
}

class _BarraPesquisarState extends State<BarraPesquisar> {
  @override
  Widget build(BuildContext context) {
    final double larguraTela = MediaQuery.of(context).size.width;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(larguraTela * 0.025),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                left: larguraTela * 0.04, right: larguraTela * 0.04),
            child: Icon(Icons.search, color: Colors.grey),
          ),
          CampoPesquisa(larguraTela: larguraTela),
          Padding(
            padding: EdgeInsets.only(
                left: larguraTela * 0.04, right: larguraTela * 0.03),
            child: PopUpUser(),
          )
        ],
      ),
    );
  }
}
