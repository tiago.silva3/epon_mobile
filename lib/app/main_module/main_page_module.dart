import 'package:epon_flutter/app/modules/cliente/cliente_controller.dart';
import 'package:epon_flutter/app/modules/equipamento/equipamento_controller.dart';
import 'package:epon_flutter/app/modules/grupo/grupo_controller.dart';
import 'package:epon_flutter/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'componentes/popup_user/popup_user_controller.dart';
import 'main_page.dart';
import 'main_page_controller.dart';

class MainPageModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => MainPageController()),
        Bind((i) => HomeController()),
        Bind((i) => ClienteController()),
        Bind((i) => EquipamentoController()),
        Bind((i) => GrupoController()),
        Bind((i) => PopUpUserController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => MainPage()),
      ];

  static Inject get to => Inject<MainPageModule>.of();
}
