import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'main_page_controller.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final controller = Modular.get<MainPageController>();
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return DefaultTabController(
        initialIndex: 0,
        length: controller.pages.length,
        child: Scaffold(
          appBar: AppBar(
            title: controller.headerApp,
            bottom: TabBar(
              onTap: controller.changeTela,
              unselectedLabelColor: Colors.grey,
              indicatorColor: controller.corIntelbras,
              labelColor: controller.corIntelbras,
              isScrollable: false,
              tabs: controller.pages.map((Pages page) {
                return Tab(
                  text: page.tabTitle,
                );
              }).toList(),
            ),
          ),
          body: controller.pages[controller.telaAtual].body,
          floatingActionButton: controller.floatButtonAdd,
        ),
      );
    });
  }
}
