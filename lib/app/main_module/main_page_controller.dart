import 'package:epon_flutter/app/main_module/componentes/barra_pesquisar/barra_pesquisar.dart';
import 'package:epon_flutter/app/main_module/componentes/nome_intelbras/nome_intelbras.dart';
import 'package:epon_flutter/app/modules/cliente/cliente_page.dart';
import 'package:epon_flutter/app/modules/equipamento/equipamento_page.dart';
import 'package:epon_flutter/app/modules/grupo/grupo_page.dart';
import 'package:epon_flutter/app/modules/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'main_page_controller.g.dart';

class MainPageController = _MainPageControllerBase with _$MainPageController;

abstract class _MainPageControllerBase with Store {
  int oldTela;

  Color corIntelbras = Color.fromRGBO(12, 122, 47, 1);

  List<Pages> pages = <Pages>[
    Pages(tabTitle: 'Home', body: HomePage()),
    Pages(tabTitle: 'Cliente', body: ClientePage()),
    Pages(tabTitle: 'Equipamento', body: EquipamentoPage()),
    Pages(tabTitle: 'Grupo', body: GrupoPage()),
  ];

  @observable
  int telaAtual = 0;

  @observable
  FloatingActionButton floatButtonAdd;

  @observable
  Widget headerApp = NomeIntelbas();

  @action
  void changeTela(int tela) {
    if (tela != oldTela) {
      telaAtual = tela;
      mudaHeader(tela);
      oldTela = tela;
    }
  }

  void mudaHeader(int tela) {
    if (tela == 0) {
      headerApp = NomeIntelbas();
      floatButtonAdd = null;
    } else {
      headerApp = BarraPesquisar();
      floatButtonAdd = FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            String foi =
                WidgetsBinding.instance.window.platformBrightness.toString();
            print(foi);
          });
    }
  }
}

class Pages {
  Pages({
    this.tabTitle,
    this.body,
  });

  final dynamic tabTitle;
  final Widget body;
}
