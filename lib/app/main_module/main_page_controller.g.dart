// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'main_page_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MainPageController on _MainPageControllerBase, Store {
  final _$telaAtualAtom = Atom(name: '_MainPageControllerBase.telaAtual');

  @override
  int get telaAtual {
    _$telaAtualAtom.context.enforceReadPolicy(_$telaAtualAtom);
    _$telaAtualAtom.reportObserved();
    return super.telaAtual;
  }

  @override
  set telaAtual(int value) {
    _$telaAtualAtom.context.conditionallyRunInAction(() {
      super.telaAtual = value;
      _$telaAtualAtom.reportChanged();
    }, _$telaAtualAtom, name: '${_$telaAtualAtom.name}_set');
  }

  final _$floatButtonAddAtom =
      Atom(name: '_MainPageControllerBase.floatButtonAdd');

  @override
  FloatingActionButton get floatButtonAdd {
    _$floatButtonAddAtom.context.enforceReadPolicy(_$floatButtonAddAtom);
    _$floatButtonAddAtom.reportObserved();
    return super.floatButtonAdd;
  }

  @override
  set floatButtonAdd(FloatingActionButton value) {
    _$floatButtonAddAtom.context.conditionallyRunInAction(() {
      super.floatButtonAdd = value;
      _$floatButtonAddAtom.reportChanged();
    }, _$floatButtonAddAtom, name: '${_$floatButtonAddAtom.name}_set');
  }

  final _$headerAppAtom = Atom(name: '_MainPageControllerBase.headerApp');

  @override
  Widget get headerApp {
    _$headerAppAtom.context.enforceReadPolicy(_$headerAppAtom);
    _$headerAppAtom.reportObserved();
    return super.headerApp;
  }

  @override
  set headerApp(Widget value) {
    _$headerAppAtom.context.conditionallyRunInAction(() {
      super.headerApp = value;
      _$headerAppAtom.reportChanged();
    }, _$headerAppAtom, name: '${_$headerAppAtom.name}_set');
  }

  final _$_MainPageControllerBaseActionController =
      ActionController(name: '_MainPageControllerBase');

  @override
  void changeTela(int tela) {
    final _$actionInfo =
        _$_MainPageControllerBaseActionController.startAction();
    try {
      return super.changeTela(tela);
    } finally {
      _$_MainPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'telaAtual: ${telaAtual.toString()},floatButtonAdd: ${floatButtonAdd.toString()},headerApp: ${headerApp.toString()}';
    return '{$string}';
  }
}
