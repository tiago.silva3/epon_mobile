import 'package:epon_flutter/app/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {
  final controller = Modular.get<AppController>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(controller.colorAppBar);
    return Observer(builder: (_) {
      return MaterialApp(
        initialRoute: '/',
        navigatorKey: Modular.navigatorKey,
        onGenerateRoute: Modular.generateRoute,
        themeMode: controller.themeMode,
        theme: controller.lightTheme,
        darkTheme: controller.darkTheme,
        debugShowCheckedModeBanner: false,
      );
    });
  }
}
