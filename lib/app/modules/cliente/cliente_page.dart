import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '_componentes/card_cliente.dart';
import 'cliente_controller.dart';

class ClientePage extends StatefulWidget {
  final String title;
  const ClientePage({Key key, this.title = "Cliente"}) : super(key: key);

  @override
  _ClientePageState createState() => _ClientePageState();
}

class _ClientePageState extends State<ClientePage> {
  final controller = Modular.get<ClienteController>();

  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      padding: EdgeInsets.only(
        bottom: tamanhoTela * 0.2,
      ),
      child: Column(
        children: controller.contentClient.map((content) {
          return Padding(
            padding: EdgeInsets.all(tamanhoTela * 0.008),
            child: CardCliente(
              cliente: content.cliente,
              contrato: content.contrato,
              servicos: content.servicos,
              entregue: content.entregue,
            ),
          );
        }).toList(),
      ),
    );
  }
}
