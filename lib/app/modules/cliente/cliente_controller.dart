import 'package:mobx/mobx.dart';

part 'cliente_controller.g.dart';

class ClienteController = _ClienteControllerBase with _$ClienteController;

abstract class _ClienteControllerBase with Store {
  List<ContentClient> contentClient = [
    ContentClient(
      contrato: "02s5s0",
      cliente: "Regina Vasconcellos",
      entregue: "80",
      servicos: ["ativo", "inativo"],
    ),
    ContentClient(
      contrato: "5895s0",
      cliente: "Abibadiel",
      entregue: "30",
      servicos: ["inatvo", "inativo"],
    ),
    ContentClient(
      contrato: "02s456",
      cliente: "Matheus Freitas",
      entregue: "60",
      servicos: ["ativo", "ativo"],
    ),
    ContentClient(
      contrato: "028526",
      cliente: "Thiago Rodrigues",
      entregue: "60",
      servicos: ["inativo", "ativo"],
    ),
    ContentClient(
      contrato: "asd545",
      cliente: "Tiago Silva",
      entregue: "60",
      servicos: ["ativo", "ativo"],
    ),
  ];
}

class ContentClient {
  ContentClient({this.cliente, this.contrato, this.entregue, this.servicos});

  final String cliente;
  final String contrato;
  final String entregue;
  final List<String> servicos;
}
