import 'package:flutter/material.dart';

import 'actioms_menu.dart';
import 'entregue.dart';
import 'servicos.dart';

class CardCliente extends StatefulWidget {
  CardCliente({
    Key key,
    this.cliente,
    this.contrato,
    this.entregue,
    this.servicos,
  }) : super(key: key);

  final String cliente;
  final String contrato;
  final String entregue;
  final List<String> servicos;

  @override
  _CardClienteState createState() => _CardClienteState();
}

class _CardClienteState extends State<CardCliente> {
  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    List contClie = [
      {"title": "Contrato: ", "content": widget.contrato},
      {"title": "Cliente: ", "content": widget.cliente},
    ];

    List servEntr = [
      {"title": "Entregue: ", "content": widget.entregue},
      {"title": "Serviços: ", "content": widget.servicos},
    ];

    return Card(
      elevation: 1,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(tamanhoTela * 0.025),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Column(
                  children: contClie.map((text) {
                return Container(
                  padding: EdgeInsets.all(tamanhoTela * 0.02),
                  width: tamanhoTela * 0.7,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(text["title"],
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      Text(text["content"] ?? ""),
                    ],
                  ),
                );
              }).toList()),
              ActionsMenu(),
              // ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: servEntr.map((content) {
              return Container(
                padding: EdgeInsets.only(
                    left: tamanhoTela * 0.02,
                    right: tamanhoTela * 0.02,
                    bottom: tamanhoTela * 0.02),
                width: tamanhoTela * 0.4,
                child: content["title"] == "Entregue: "
                    ? Entregue(content: content)
                    : Servicos(content: content, tamanhoTela: tamanhoTela),
              );
            }).toList(),
          ),
          LinearProgressIndicator(
            value: double.parse(widget.entregue) / 100,
          ),
        ],
      ),
    );
  }
}
