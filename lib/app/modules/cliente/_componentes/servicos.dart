import 'package:flutter/material.dart';

class Servicos extends StatefulWidget {
  Servicos({Key key, this.content, this.tamanhoTela}) : super(key: key);

  final double tamanhoTela;

  final dynamic content;

  @override
  _ServicosState createState() => _ServicosState();
}

class _ServicosState extends State<Servicos> {
  @override
  Widget build(BuildContext context) {
    List<Color> colors = verifcaServico(widget.content["content"]);
    return Row(
      children: <Widget>[
        Text(
          widget.content["title"],
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: widget.tamanhoTela * 0.02,
                right: widget.tamanhoTela * 0.01,
              ),
              child: Icon(
                Icons.phone,
                color: colors[0] ?? Colors.grey,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: widget.tamanhoTela * 0.02,
                right: widget.tamanhoTela * 0.01,
              ),
              child: Icon(
                Icons.device_hub,
                color: colors[1] ?? Colors.grey,
              ),
            )
          ],
        ),
      ],
    );
  }

  List<Color> verifcaServico(List servicos) {
    if (servicos == null || servicos.isEmpty) {
      return <Color>[Colors.green, Colors.red];
    }

    return servicos.map((serv) {
      switch (serv) {
        case "inativo":
          return Colors.red;
          break;
        default:
          return Colors.green;
          break;
      }
    }).toList();
  }
}
