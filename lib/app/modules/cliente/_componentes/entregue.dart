import 'package:flutter/material.dart';

class Entregue extends StatefulWidget {
  Entregue({Key key, this.content}) : super(key: key);

  final dynamic content;

  @override
  _EntregueState createState() => _EntregueState();
}

class _EntregueState extends State<Entregue> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
          widget.content["title"],
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          (widget.content["content"] ?? "") + " mb",
        ),
      ],
    );
  }
}
