import 'package:mobx/mobx.dart';

part 'grupo_controller.g.dart';

class GrupoController = _GrupoControllerBase with _$GrupoController;

abstract class _GrupoControllerBase with Store {
  @observable
  List<ContentGrupo> contentGrupo = [
    ContentGrupo(
      ativos: 5,
      inativos: 2,
      bloqueados: 0,
      grupo: "1",
      clientes: 7,
      capacidade: [200, 10],
    ),
    ContentGrupo(
      ativos: 0,
      inativos: 2,
      bloqueados: 0,
      grupo: "2",
      clientes: 2,
      capacidade: [300, 10],
    ),
    ContentGrupo(
      ativos: 1,
      inativos: 2,
      bloqueados: 2,
      grupo: "3",
      clientes: 5,
      capacidade: [40, 50],
    ),
    ContentGrupo(
      ativos: 0,
      inativos: 5,
      bloqueados: 0,
      grupo: "4",
      clientes: 5,
      capacidade: [200, 30],
    ),
    ContentGrupo(
      ativos: 0,
      inativos: 2,
      bloqueados: 5,
      grupo: "5",
      clientes: 7,
      capacidade: [80, 40],
    ),
  ];
}

class ContentGrupo {
  ContentGrupo({
    this.grupo,
    this.ativos,
    this.inativos,
    this.bloqueados,
    this.clientes,
    this.capacidade,
  });

  final int ativos;
  final int inativos;
  final int bloqueados;
  final String grupo;
  final int clientes;
  final List<int> capacidade;
}
