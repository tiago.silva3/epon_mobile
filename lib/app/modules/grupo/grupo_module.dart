import 'package:flutter_modular/flutter_modular.dart';

import 'grupo_controller.dart';
import 'grupo_page.dart';

class GrupoModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => GrupoController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => GrupoPage()),
      ];

  static Inject get to => Inject<GrupoModule>.of();
}
