import 'package:flutter/material.dart';

class GrupoBloqueados extends StatefulWidget {
  GrupoBloqueados({Key key, this.bloqueados, this.tamanhoTela})
      : super(key: key);

  final int bloqueados;
  final double tamanhoTela;

  @override
  _GrupoBloqueadosState createState() => _GrupoBloqueadosState();
}

class _GrupoBloqueadosState extends State<GrupoBloqueados> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.tamanhoTela * 0.8,
        padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.brightness_1,
              color: Colors.grey,
              size: widget.tamanhoTela * 0.035,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  "  Bloqueados: ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text("${widget.bloqueados}" ?? ""),
              ],
            ),
          ],
        ));
  }
}
