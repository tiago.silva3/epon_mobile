import 'package:flutter/material.dart';

class CapacaidadeGrupo extends StatefulWidget {
  CapacaidadeGrupo({Key key, this.capacidade, this.tamanhoTela})
      : super(key: key);

  final List<int> capacidade;
  final double tamanhoTela;

  @override
  _CapacaidadeGrupoState createState() => _CapacaidadeGrupoState();
}

class _CapacaidadeGrupoState extends State<CapacaidadeGrupo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.tamanhoTela * 0.8,
      padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            "Capacidade restantes: ${widget.capacidade[0] ?? ''} clientes a ${widget.capacidade[1] ?? ''} mb.",
          ),
        ],
      ),
    );
  }
}
