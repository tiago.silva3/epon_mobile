import 'package:flutter/material.dart';

class GrupoTitle extends StatefulWidget {
  GrupoTitle({Key key, this.grupo, this.tamanhoTela}) : super(key: key);

  final String grupo;
  final double tamanhoTela;

  @override
  _GrupoTitleState createState() => _GrupoTitleState();
}

class _GrupoTitleState extends State<GrupoTitle> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.tamanhoTela * 0.8,
        padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  "Grupo ",
                  style: TextStyle(
                    fontSize: widget.tamanhoTela * 0.045,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  widget.grupo ?? "",
                  style: TextStyle(fontSize: widget.tamanhoTela * 0.045),
                ),
              ],
            ),
          ],
        ));
  }
}
