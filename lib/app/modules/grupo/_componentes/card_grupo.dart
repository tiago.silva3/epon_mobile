import 'package:epon_flutter/app/modules/grupo/_componentes/action_grupo.dart';
import 'package:flutter/material.dart';

import 'capacidade.dart';
import 'count_cliente.dart';
import 'grupo_ativos.dart';
import 'grupo_bloqueados.dart';
import 'grupo_inativos.dart';
import 'grupo_title.dart';

class CardGrupo extends StatefulWidget {
  CardGrupo({
    Key key,
    this.ativos,
    this.inativos,
    this.bloqueados,
    this.grupo,
    this.clientes,
    this.capacidade,
  }) : super(key: key);

  final int ativos;
  final int inativos;
  final int bloqueados;
  final int clientes;
  final List<int> capacidade;
  final String grupo;

  @override
  _CardGrupoState createState() => _CardGrupoState();
}

class _CardGrupoState extends State<CardGrupo> {
  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    List content = [
      {"title": "Grupo", "content": widget.grupo},
      {"title": "Ativos: ", "content": widget.ativos},
      {"title": "Inativos: ", "content": widget.inativos},
      {"title": "Bloqueados: ", "content": widget.bloqueados},
      {"title": "Clientes: ", "content": widget.clientes},
      {"title": "Capacidade: ", "content": widget.capacidade},
    ];

    return Card(
      elevation: 1,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(tamanhoTela * 0.025),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: content.map((text) {
                  switch (text["title"]) {
                    case "Ativos: ":
                      return GrupoAtivos(
                        ativos: text["content"],
                        tamanhoTela: tamanhoTela,
                      );
                      break;

                    case "Inativos: ":
                      return GrupoInativos(
                        inativos: text["content"],
                        tamanhoTela: tamanhoTela,
                      );
                      break;

                    case "Bloqueados: ":
                      return GrupoBloqueados(
                        bloqueados: text["content"],
                        tamanhoTela: tamanhoTela,
                      );
                      break;

                    case "Clientes: ":
                      return CountCliente(
                        clientes: content[4]["content"],
                        tamanhoTela: tamanhoTela,
                      );
                      break;

                    case "Capacidade: ":
                      return CapacaidadeGrupo(
                        capacidade: content[5]["content"],
                        tamanhoTela: tamanhoTela,
                      );
                      break;

                    default:
                      return GrupoTitle(
                        grupo: text["content"],
                        tamanhoTela: tamanhoTela,
                      );
                  }
                }).toList(),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: tamanhoTela * 0.05, bottom: tamanhoTela * 0.37),
                child: ActionGrupo(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
