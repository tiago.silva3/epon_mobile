import 'package:flutter/material.dart';

class GrupoAtivos extends StatefulWidget {
  GrupoAtivos({Key key, this.ativos, this.tamanhoTela}) : super(key: key);

  final int ativos;
  final double tamanhoTela;

  @override
  _GrupoAtivosState createState() => _GrupoAtivosState();
}

class _GrupoAtivosState extends State<GrupoAtivos> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.tamanhoTela * 0.8,
        padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.brightness_1,
              color: Colors.green,
              size: widget.tamanhoTela * 0.035,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  "  Ativos: ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text("${widget.ativos}" ?? ""),
              ],
            ),
          ],
        ));
  }
}
