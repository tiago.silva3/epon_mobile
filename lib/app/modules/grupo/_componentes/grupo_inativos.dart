import 'package:flutter/material.dart';

class GrupoInativos extends StatefulWidget {
  GrupoInativos({Key key, this.inativos, this.tamanhoTela}) : super(key: key);

  final int inativos;
  final double tamanhoTela;

  @override
  _GrupoInativosState createState() => _GrupoInativosState();
}

class _GrupoInativosState extends State<GrupoInativos> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.tamanhoTela * 0.8,
        padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.brightness_1,
              color: Colors.red,
              size: widget.tamanhoTela * 0.035,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  "  Inativos: ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text("${widget.inativos}" ?? ""),
              ],
            ),
          ],
        ));
  }
}
