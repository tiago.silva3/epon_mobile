import 'package:flutter/material.dart';

class CountCliente extends StatefulWidget {
  CountCliente({Key key, this.clientes, this.tamanhoTela}) : super(key: key);

  final int clientes;
  final double tamanhoTela;

  @override
  _CountClienteState createState() => _CountClienteState();
}

class _CountClienteState extends State<CountCliente> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.tamanhoTela * 0.8,
      padding: EdgeInsets.all(widget.tamanhoTela * 0.02),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            "Clientes: ",
          ),
          Text("${widget.clientes ?? ''}"),
        ],
      ),
    );
  }
}
