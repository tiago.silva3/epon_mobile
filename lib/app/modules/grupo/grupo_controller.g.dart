// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'grupo_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GrupoController on _GrupoControllerBase, Store {
  final _$contentGrupoAtom = Atom(name: '_GrupoControllerBase.contentGrupo');

  @override
  List<ContentGrupo> get contentGrupo {
    _$contentGrupoAtom.context.enforceReadPolicy(_$contentGrupoAtom);
    _$contentGrupoAtom.reportObserved();
    return super.contentGrupo;
  }

  @override
  set contentGrupo(List<ContentGrupo> value) {
    _$contentGrupoAtom.context.conditionallyRunInAction(() {
      super.contentGrupo = value;
      _$contentGrupoAtom.reportChanged();
    }, _$contentGrupoAtom, name: '${_$contentGrupoAtom.name}_set');
  }

  @override
  String toString() {
    final string = 'contentGrupo: ${contentGrupo.toString()}';
    return '{$string}';
  }
}
