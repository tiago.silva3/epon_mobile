import 'package:epon_flutter/app/modules/grupo/_componentes/card_grupo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'grupo_controller.dart';

class GrupoPage extends StatefulWidget {
  @override
  _GrupoPageState createState() => _GrupoPageState();
}

class _GrupoPageState extends State<GrupoPage> {
  final controller = Modular.get<GrupoController>();

  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      padding: EdgeInsets.only(
          top: tamanhoTela * 0.005,
          left: tamanhoTela * 0.005,
          right: tamanhoTela * 0.005,
          bottom: tamanhoTela * 0.2),
      child: Observer(builder: (_) {
        return Column(
          mainAxisSize: MainAxisSize.max,
          children: controller.contentGrupo.map((content) {
            return CardGrupo(
                grupo: content.grupo,
                ativos: content.ativos,
                inativos: content.inativos,
                bloqueados: content.bloqueados,
                clientes: content.clientes,
                capacidade: content.capacidade);
          }).toList(),
        );
      }),
    );
  }
}
