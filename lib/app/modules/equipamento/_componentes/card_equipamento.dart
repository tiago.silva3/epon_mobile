import 'package:flutter/material.dart';

import 'actioms_menu.dart';

class CardEquipamento extends StatefulWidget {
  CardEquipamento({
    Key key,
    this.nome,
    this.ip,
    this.porta,
  }) : super(key: key);

  final String nome;
  final String ip;
  final String porta;

  @override
  _CardEquipamentoState createState() => _CardEquipamentoState();
}

class _CardEquipamentoState extends State<CardEquipamento> {
  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    List content = [
      {"title": "Nome: ", "content": widget.nome},
      {"title": "IP: ", "content": widget.ip},
      {"title": "Porta: ", "content": widget.porta},
    ];

    return Card(
      elevation: 1,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(tamanhoTela * 0.025),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Column(
              mainAxisSize: MainAxisSize.min,
              children: content.map((text) {
                return Padding(
                  padding: EdgeInsets.only(
                    top: tamanhoTela * 0.02,
                    left: tamanhoTela * 0.02,
                    right: tamanhoTela * 0.02,
                    bottom: tamanhoTela * 0.02,
                  ),
                  child: Container(
                      width: tamanhoTela * 0.765,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            text["title"],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(text["content"] ?? "")
                        ],
                      )),
                );
              }).toList()),
          Padding(
            padding: EdgeInsets.only(bottom: tamanhoTela * 0.11),
            child: ActionsMenu(),
          ),
          // ),
        ],
      ),
    );
  }
}
