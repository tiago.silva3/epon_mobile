import 'package:flutter/material.dart';

class ActionsMenu extends StatefulWidget {
  @override
  _ActionsMenuState createState() => _ActionsMenuState();
}

class _ActionsMenuState extends State<ActionsMenu> {
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      icon: Icon(Icons.more_vert),
      elevation: 1,
      style: TextStyle(color: Colors.green),
      underline: Container(
        height: 0,
      ),
      items: <String>['Editar', 'Excluir']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value,
          ),
        );
      }).toList(),
      onChanged: (foi) {},
    );
  }
}
