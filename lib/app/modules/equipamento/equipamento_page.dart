import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '_componentes/card_equipamento.dart';
import 'equipamento_controller.dart';

class EquipamentoPage extends StatefulWidget {
  final String title;
  const EquipamentoPage({Key key, this.title = "Equipamento"})
      : super(key: key);

  @override
  _EquipamentoPageState createState() => _EquipamentoPageState();
}

class _EquipamentoPageState extends State<EquipamentoPage> {
  final controller = Modular.get<EquipamentoController>();
  @override
  Widget build(BuildContext context) {
    double tamanhoTela = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
        padding: EdgeInsets.only(
          top: tamanhoTela * 0.005,
          left: tamanhoTela * 0.005,
          right: tamanhoTela * 0.005,
          bottom: tamanhoTela * 0.2,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: controller.contentEquipamento.map((eqp) {
            return CardEquipamento(
                nome: eqp.nome, ip: eqp.ip, porta: eqp.porta);
          }).toList(),
        ));
  }
}
