import 'package:flutter_modular/flutter_modular.dart';

import 'equipamento_controller.dart';
import 'equipamento_page.dart';

class EquipamentoModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => EquipamentoController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => EquipamentoPage()),
      ];

  static Inject get to => Inject<EquipamentoModule>.of();
}
