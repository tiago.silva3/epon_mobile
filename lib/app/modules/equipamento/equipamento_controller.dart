import 'package:mobx/mobx.dart';

part 'equipamento_controller.g.dart';

class EquipamentoController = _EquipamentoControllerBase
    with _$EquipamentoController;

abstract class _EquipamentoControllerBase with Store {
  List<ContentEquipamento> contentEquipamento = [
    ContentEquipamento(
      nome: "Equipamento 1",
      ip: "000.111.000.000",
      porta: "0132",
    ),
    ContentEquipamento(
      nome: "Equipamento 2",
      ip: "000.222.000.000",
      porta: "4560",
    ),
    ContentEquipamento(
      nome: "Equipamento 3",
      ip: "000.000.333.000",
      porta: "7980",
    ),
    ContentEquipamento(
      nome: "Equipamento 4",
      ip: "000.000.000.555",
      porta: "0654",
    ),
    ContentEquipamento(
      nome: "Equipamento 5",
      ip: "666.000.000.000",
      porta: "0369",
    ),
  ];
}

class ContentEquipamento {
  ContentEquipamento({this.nome, this.ip, this.porta});

  final String nome;
  final String ip;
  final String porta;
}
