import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = Modular.get<HomeController>();
  @override
  Widget build(BuildContext context) {
    final double tamnhoTela = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(
          top: tamnhoTela * 0.02,
          bottom: tamnhoTela * 0.04,
        ),
        child: Column(
            children: controller.textos.map((texto) {
          return Padding(
              padding: EdgeInsets.only(
                top: tamnhoTela * 0.01,
                left: tamnhoTela * 0.02,
                right: tamnhoTela * 0.02,
              ),
              child: Card(
                  child: ListTile(
                title: Text(texto.title),
                subtitle: Text(texto.text),
              )));
        }).toList()),
      ),
    );
  }
}
